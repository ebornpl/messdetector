/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package steidel.przybylska;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import steidel.przybylska.cropimage.CropImage;

/**
 *
 * @author Paulinka
 */
public class MessPreviewActivity extends Activity implements View.OnClickListener, ResponseProblemDialog.ResponseProblemListener {

    public static final int MESS_PREV_CLOSED = 303;
    public static final int CROPPING_ACTIVITY= 0;
    Button internetButton;
    Button calibrationButton;
    Button donothingButton;
    ImageView imageView;
    int dialogFunc;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.messpreview);

        internetButton = (Button) findViewById(R.id.internetButton);
        calibrationButton = (Button) findViewById(R.id.calibrationButton);
        donothingButton = (Button) findViewById(R.id.donothingButton);

        internetButton.setOnClickListener(this);
        calibrationButton.setOnClickListener(this);
        donothingButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imageView = (ImageView) findViewById(R.id.imageView);
        Bitmap img = BitmapFactory.decodeByteArray(
                getIntent().getByteArrayExtra(MainActivity.MESS_EXTRA_MSG),
                0, getIntent().getByteArrayExtra(MainActivity.MESS_EXTRA_MSG).length);
        imageView.setImageBitmap(img);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.internetButton:
                blockInternet();
                break;
            case R.id.calibrationButton:
                calibrateKinect();
                break;
            case R.id.donothingButton:
                Toast.makeText(this, "Everything seems to be OK", Toast.LENGTH_LONG).show();
                setResult(MESS_PREV_CLOSED);
                finish();
                break;
        }
    }

    public boolean checkInternetConnection() {
        ConnectivityManager mConnManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetInfo = mConnManager.getActiveNetworkInfo();
        return (mNetInfo != null && mNetInfo.isConnected());
    }

    public void blockInternet() {
        new InternetConnectionManager(this, null).execute(InternetConnectionManager.BLOCK_INTERNET);
    }

    public void calibrateKinect() {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra("image-bytes", getIntent().getByteArrayExtra(MainActivity.MESS_EXTRA_MSG));
        intent.putExtra("scale", true);
        startActivityForResult(intent,CROPPING_ACTIVITY);
    }

    public void showResponseProblemDialog(int func) {
        dialogFunc = func;
        DialogFragment newFragment = new ResponseProblemDialog();
        newFragment.show(getFragmentManager(), "responseproblem");
    }

    public void onDialogPositiveClick(DialogFragment dialog) {
        if (checkInternetConnection()) {
            if (dialogFunc == 1) {
                calibrateKinect();
            } else if (dialogFunc == 2) {
                blockInternet();
            }
        } else {
            Toast.makeText(this, "No Internet connection\nEstablish connection and try again. ", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.dismiss();
        recreate();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CROPPING_ACTIVITY && resultCode == RESULT_OK) {
            Rect selected=data.getParcelableExtra("rect");
            new InternetConnectionManager(this, null).execute(InternetConnectionManager.CALIBRATE_KINECT, selected);
        }
    }
}
