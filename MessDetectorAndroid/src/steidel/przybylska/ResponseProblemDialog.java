/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package steidel.przybylska;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

/**
 *
 * @author Paulinka
 */
public class ResponseProblemDialog extends DialogFragment {

    public interface ResponseProblemListener {

        public void onDialogPositiveClick(DialogFragment dialog);

        public void onDialogNegativeClick(DialogFragment dialog);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        mBuilder.setMessage(R.string.dialog_response_problem)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface di, int i) {
                mListener.onDialogPositiveClick(ResponseProblemDialog.this);
            }
        })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface di, int i) {
                mListener.onDialogNegativeClick(ResponseProblemDialog.this);
            }
        })
                .setNeutralButton(R.string.setserver, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                setServerAddress();
            }
        });
        return mBuilder.create();
    }
    ResponseProblemListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (ResponseProblemListener) activity;
    }

    private void setServerAddress() {
        Intent serverSettingsIntent = new Intent(getActivity(), ServerSettingsActivity.class);
        startActivity(serverSettingsIntent);
    }
}
