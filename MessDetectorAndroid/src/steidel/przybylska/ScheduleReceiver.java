/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package steidel.przybylska;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;

/**
 *
 * @author Paulinka
 */
public class ScheduleReceiver extends BroadcastReceiver {

    private static final long REPEAT_TIME = 1000 * 30;
    private static String roomState = "";

    public static void setRoomState(String state) {
        roomState = state;
    }

    public static String getRoomState() {
        return roomState;
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();
        Log.d("==MESS==", "w schedulerze przed zapytaniem");
        
        if(checkInternetConnection(context)) {
            Log.d("==MESS==", "w schedulerze server_address_base = " + InternetConnectionManager.getSERVER_ADDRESS_BASE());
            Log.d("==MESS==", "w schedulerze przed zapytaniem + z polaczeniem");
            new InternetConnectionManager(null, context).execute(InternetConnectionManager.CHECK_STATUS, roomState);
        }

        wl.release();
    }

    public static void setAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, ScheduleReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), REPEAT_TIME, pi); // Millisec * Second * Minute
    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, ScheduleReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
    
     public boolean checkInternetConnection(Context context) {
        ConnectivityManager mConnManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetInfo = mConnManager.getActiveNetworkInfo();
        return (mNetInfo != null && mNetInfo.isConnected());
    }
}
