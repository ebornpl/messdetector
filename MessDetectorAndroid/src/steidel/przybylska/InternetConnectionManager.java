/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package steidel.przybylska;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import static android.content.Context.NOTIFICATION_SERVICE;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Paulinka
 */
public class InternetConnectionManager extends AsyncTask {

    private int func = 0;
    private final Activity mParentActivity;
    private Context mContext = null;
    public final static String CALIBRATE_KINECT = "CALIBRATE_KINECT";
    public final static String BLOCK_INTERNET = "BLOCK_INTERNET";
    public final static String CHECK_CURRENT = "CHECK_CURRENT";
    public final static String CHECK_STATUS = "CHECK_STATUS";
    public final static String GET_PICTURE = "GET_PICTURE";
    private static String SERVER_ADDRESS_BASE="aaa";
    private static final String URL_IMAGE = "/helloworld/image";
    private static final String URL_CALIBRATION = "/helloworld/calibrate";
    private static final String URL_STATUS = "/helloworld/status";
    private static final String URL_BLOCK = "/helloworld/lock";

    public InternetConnectionManager(Activity activity, Context context) {
        this.mParentActivity = activity;
        this.mContext = context;
    }

    public static String getSERVER_ADDRESS_BASE() {
        return SERVER_ADDRESS_BASE;
    }

    public static void setSERVER_ADDRESS_BASE(String SERVER_ADDRESS_BASE) {
        InternetConnectionManager.SERVER_ADDRESS_BASE = "http://"+SERVER_ADDRESS_BASE + ":9998";
        Log.d("==MESS==", "server in set: " + InternetConnectionManager.SERVER_ADDRESS_BASE);
    }

    private void prepareNotification(String result) {
        new InternetConnectionManager(null, mContext).execute(InternetConnectionManager.GET_PICTURE);
    }

    private void showNotification(byte[] image) {
        Log.d("==MESS==", "w internecie przed notifem");
        NotificationManager mNM = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);

        Notification notification = new Notification(R.drawable.messdetector_ic, ScheduleReceiver.getRoomState(),
                System.currentTimeMillis());

        Intent messPreviewIntent = new Intent(mContext, MessPreviewActivity.class);
        messPreviewIntent.putExtra(MainActivity.MESS_EXTRA_MSG, image);

        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, messPreviewIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notification.setLatestEventInfo(mContext, mContext.getText(R.string.app_name),
                ScheduleReceiver.getRoomState(), contentIntent);

        mNM.notify(R.string.app_name, notification);
        Log.d("==MESS==", "w internecie po shownotif");
    }

    public void showToastMessage(String result) {
        Toast.makeText(mParentActivity, result, Toast.LENGTH_LONG).show();
    }

    public void showResponseProblemDialog() {
        Log.d("==MESS==", "w internecie w problemieshow");
        if (mParentActivity != null) {
            DialogFragment newFragment = new ResponseProblemDialog();
            newFragment.show(mParentActivity.getFragmentManager(), "responseproblem");
        }
    }

    @Override
    protected Object doInBackground(Object... paramss) {
        Log.d("==MESS==", "w back, params[0]= " + paramss[0].toString());
        try {
            if (paramss[0].toString().equals(CALIBRATE_KINECT)) {
                func = 1;
                Rect selected = (Rect) paramss[1];
                String query = URL_CALIBRATION
                        + "?left=" + selected.left
                        + "&right=" + selected.right
                        + "&top=" + selected.top
                        + "&bottom=" + selected.bottom;
                Log.d("mess", query);
                return manageMessDetectorRemotly(query);
            } else if (paramss[0].toString().equals(BLOCK_INTERNET)) {
                func = 2;
                return manageMessDetectorRemotly(URL_BLOCK);
            } else if (paramss[0].toString().equals(CHECK_CURRENT)) {
                func = 3;
                return manageMessDetectorRemotly(URL_IMAGE);
            } else if (paramss[0].toString().equals(CHECK_STATUS)) {
                func = 4;
                return manageMessDetectorRemotly(URL_STATUS, paramss[1].toString());
            } else if (paramss[0].toString().equals(GET_PICTURE)) {
                func = 5;
                return manageMessDetectorRemotly(URL_IMAGE);
            } else {
                return null;
            }
        } catch (MalformedURLException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    private Object manageMessDetectorRemotly(String... url) throws MalformedURLException, IOException {
        InputStream mInStream = null;
        Log.d("==MESS==", "w mngr func = "  + func);
        Log.d("==MESS==", "w mngr url[0]=" + url[0]);

        try {
            Log.d("==MESS==", "w mngr na poczatku trya");
            String s = SERVER_ADDRESS_BASE + url[0];
            Log.d("==MESS==", "s: " + s);
            URL mUrl = new URL(s);
            Log.d("==MESS==", mUrl.toString());
            
            HttpURLConnection mConnection = (HttpURLConnection) mUrl.openConnection();
            mConnection.setReadTimeout(5000);
            mConnection.setConnectTimeout(15000);
            mConnection.setRequestMethod("GET");
            mConnection.setDoInput(true);

            mConnection.connect();
            int mResponse = mConnection.getResponseCode();
            mInStream = mConnection.getInputStream();

            
            Log.d("==MESS==", "w managerze po connection przed ifami");
            if (func == 1 || func == 2) {
                Log.d("==MESS==", "w mngr w if 1 2");
                String contentAsString = convertInputStreamToString(mInStream);
                return contentAsString;
            } else if (func == 3 || func == 5) {
                Log.d("==MESS==", "w mngr w if 3 5");
                BitmapFactory.Options mBitmapOptions = new BitmapFactory.Options();
                mBitmapOptions.inSampleSize = 1;
                Bitmap mCurrentImage = BitmapFactory.decodeStream(mInStream, null, mBitmapOptions);

                return mCurrentImage;
            } else if (func == 4) {
                Log.d("==MESS==", "w mngr w if 4");
                String contentAsString = convertInputStreamToString(mInStream);
                Log.d("==MESS==", "w mngr w 4 url[1]" + url[1]);
                Log.d("==MESS==", "w mngr w 4 contentas...=" + contentAsString);
                if (!contentAsString.equals(url[1])) {
                    Log.d("==MESS==", "w mngr w if 4 w extra if");
                    ScheduleReceiver.setRoomState(contentAsString);
                    return contentAsString;
                }
            }
            return null;

        } finally {
            Log.d("==MESS==", "w finally");
            if (mInStream != null) {
                mInStream.close();
            }
        }
    }

    private String convertInputStreamToString(InputStream mInStream) throws UnsupportedEncodingException, IOException {
        Reader mReader = new InputStreamReader(mInStream, "UTF-8");
        char[] buffer = new char[200];
        mReader.read(buffer);
        return new String(buffer);
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if (result != null) {
            switch (func) {
                case 1:
                    if (!result.toString().isEmpty() && mParentActivity != null) {
                        showToastMessage(result.toString());
                    }
                    break;
                case 2:
                    if (!result.toString().isEmpty() && mParentActivity != null) {
                        showToastMessage(result.toString());
                    }
                    break;
                case 3:
                    if (mParentActivity != null) {
                        Bitmap img = (Bitmap) result;
                        ByteArrayOutputStream mBAOutStream = new ByteArrayOutputStream();
                        img.compress(Bitmap.CompressFormat.PNG, 50, mBAOutStream);

                        Intent messPreviewIntent = new Intent(mParentActivity.getApplicationContext(), MessPreviewActivity.class);
                        messPreviewIntent.putExtra(MainActivity.MESS_EXTRA_MSG, mBAOutStream.toByteArray());
                        mParentActivity.startActivityForResult(messPreviewIntent, MainActivity.MESS_PREVIEW_ACTIVITY);
                    }
                    break;
                case 4:
                    if (!result.toString().isEmpty() && mContext != null) {
                        Log.d("==MESS==", "w internecie w poscie 4");
                        prepareNotification(result.toString());
                    }
                    break;
                case 5:
                    if (mContext != null) {
                        Log.d("==MESS==", "w internecie w poscie 5");
                        Bitmap img = (Bitmap) result;
                        ByteArrayOutputStream mBAOutStream = new ByteArrayOutputStream();
                        img.compress(Bitmap.CompressFormat.PNG, 50, mBAOutStream);
                        showNotification(mBAOutStream.toByteArray());
                    }
                    break;
            }
        } else {
            Log.d("==MESS==", "w poscie w elsie " + result);
            showResponseProblemDialog();
        }
    }
}