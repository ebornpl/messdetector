package steidel.przybylska;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ServerSettingsActivity extends Activity implements View.OnClickListener {

    Button saveButton;
    public static final int SERVER_SETTINGS_CLOSED = 404;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.serversettings);
        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        InternetConnectionManager.setSERVER_ADDRESS_BASE(((EditText) findViewById(R.id.addressText)).getText().toString());
        setResult(SERVER_SETTINGS_CLOSED);
        finish();
    }
}
