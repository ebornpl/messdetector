package steidel.przybylska;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener, ResponseProblemDialog.ResponseProblemListener {

    Button checkButton;
    public final static String MESS_EXTRA_MSG = "MESS.imgmsg";
    public final static int MESS_PREVIEW_ACTIVITY = 33;
    public final static int SERVER_SETTINGS_ACTIVITY = 44;
//    private MessDetectorService mService;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        checkButton = (Button) findViewById(R.id.checkButton);
        checkButton.setOnClickListener(this);
        ScheduleReceiver.setAlarm(this);
//        startService(new Intent(this, MessDetectorService.class));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.checkButton:
                if (checkInternetConnection()) {
                    if (InternetConnectionManager.getSERVER_ADDRESS_BASE() == null || InternetConnectionManager.getSERVER_ADDRESS_BASE().isEmpty()) {
                        setServerAddress();
                    } else {
                        getCurrentPicture();
                    }
                } else {
                    Toast.makeText(this, "No Internet connection\nEstablish connection and try again. ", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    public boolean checkInternetConnection() {
        ConnectivityManager mConnManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetInfo = mConnManager.getActiveNetworkInfo();
        return (mNetInfo != null && mNetInfo.isConnected());
    }

    private void getCurrentPicture() {
        new InternetConnectionManager(this, null).execute(InternetConnectionManager.CHECK_CURRENT);
    }

    public void onDialogPositiveClick(DialogFragment dialog) {
        if (checkInternetConnection()) {
            getCurrentPicture();
        } else {
            Toast.makeText(this, "No Internet connection or wrong server address\n"
                    + "Establish connection or/and change server settings and try again. ", 
                    Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.dismiss();
        recreate();
    }

    public void showResponseProblemDialog() {
        DialogFragment newFragment = new ResponseProblemDialog();
        newFragment.show(getFragmentManager(), "responseproblem");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MESS_PREVIEW_ACTIVITY && resultCode == MessPreviewActivity.MESS_PREV_CLOSED) {
            finish();
        } else if(requestCode == SERVER_SETTINGS_ACTIVITY && resultCode == ServerSettingsActivity.SERVER_SETTINGS_CLOSED) {
            Toast.makeText(this, "Settings saved. ", Toast.LENGTH_LONG).show();
            getCurrentPicture();
        }
    }

    private void setServerAddress() {
        Intent serverSettingsIntent = new Intent(this, ServerSettingsActivity.class);
        startActivityForResult(serverSettingsIntent, ServerSettingsActivity.SERVER_SETTINGS_CLOSED);
    }
}
