package steidel.przybylska;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 * User: eborn
 * Date: 2/3/13
 * Time: 12:16 AM
 * To change this template use File | Settings | File Templates.
 */
// The Java class will be hosted at the URI path "/helloworld"
@Path("/helloworld")
public class RequestHandler {

    @GET
    @Produces("text/plain")
    @Path("/calibrate")
    public String calibrate(@DefaultValue("0") @QueryParam("left") int left,
                            @DefaultValue("640") @QueryParam("right") int right,
                            @DefaultValue("0") @QueryParam("top") int top,
                            @DefaultValue("480") @QueryParam("bottom") int bottom) {
        SimpleViewerApplication.getViewer().calibrate(left,right,top,bottom);
        return "calibrated.";
    }

    @GET
    @Produces("image/png")
    @Path("/image")
    public Response getImage() {

        return Response.ok(SimpleViewerApplication.getViewer().getBimg()).build();
    }

    @GET
    @Produces("text/plain")
    @Path("/status")
    public String getStatus() {
        if (SimpleViewerApplication.getViewer().isClean())
            return "clean";
        else
            return "mess";
    }

    @GET
    @Produces("text/plain")
    @Path("/lock")
    public String lock() {
        return SimpleViewerApplication.lockComputer();
    }
}