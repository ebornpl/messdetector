/****************************************************************************
 *                                                                           *
 *  OpenNI 1.x Alpha                                                         *
 *  Copyright (C) 2011 PrimeSense Ltd.                                       *
 *                                                                           *
 *  This file is part of OpenNI.                                             *
 *                                                                           *
 *  OpenNI is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU Lesser General Public License as published *
 *  by the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  OpenNI is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             *
 *  GNU Lesser General Public License for more details.                      *
 *                                                                           *
 *  You should have received a copy of the GNU Lesser General Public License *
 *  along with OpenNI. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                           *
 ****************************************************************************/
package steidel.przybylska;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class SimpleViewerApplication {
    private static SimpleViewerCustom viewer;
    private static JFrame frame;
    private boolean shouldRun = true;

    public SimpleViewerApplication(JFrame frame) {
        {
            this.frame = frame;
            frame.addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent arg0) {
                }

                @Override
                public void keyReleased(KeyEvent arg0) {
                }

                @Override
                public void keyPressed(KeyEvent arg0) {
                    if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        shouldRun = false;
                    }
                    if (arg0.getKeyCode() == KeyEvent.VK_A) {
                        viewer.calibrate(0, 640, 0, 480);
                    }
                }
            });
            frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    shouldRun = false;
                }
            });
        }
    }

    public static void main(String s[]) throws IOException {
        JFrame f = new JFrame("OpenNI Simple Viewer");
        SimpleViewerApplication app = new SimpleViewerApplication(f);

        app.viewer = new SimpleViewerCustom();
        f.add("Center", app.viewer);
        f.pack();
        f.setVisible(true);
        HttpServer server = HttpServerFactory.create("http://localhost:9998/");
        server.start();

        System.out.println("Server running");
        System.out.println("Visit: http://localhost:9998/helloworld");
        app.run();
        server.stop(0);
    }

    public static SimpleViewerCustom getViewer() {
        return viewer;
    }

    public static String lockComputer() {
        if (frame.isAlwaysOnTop()) {
            frame.dispose();
            frame.add("Center", viewer);
            frame.setUndecorated(false);
            frame.setAlwaysOnTop(false);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
            viewer.lock(false);
            return "unlocked";
        } else {
            frame.dispose();
            frame.add("Center", viewer);
            frame.setUndecorated(true);
            frame.setAlwaysOnTop(true);
            frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
            frame.setExtendedState(Frame.MAXIMIZED_BOTH);
            viewer.lock(true);
            return "locked";
        }
    }

    void run() {
        while (shouldRun) {
            viewer.updateDepth();
            viewer.repaint();
        }
        frame.dispose();
    }
}
