/****************************************************************************
 *                                                                           *
 *  OpenNI 1.x Alpha                                                         *
 *  Copyright (C) 2011 PrimeSense Ltd.                                       *
 *                                                                           *
 *  This file is part of OpenNI.                                             *
 *                                                                           *
 *  OpenNI is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU Lesser General Public License as published *
 *  by the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  OpenNI is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             *
 *  GNU Lesser General Public License for more details.                      *
 *                                                                           *
 *  You should have received a copy of the GNU Lesser General Public License *
 *  along with OpenNI. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                           *
 ****************************************************************************/
package steidel.przybylska;

import org.OpenNI.*;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.*;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

class SimpleViewerCustom extends Component {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final String SAMPLE_XML_FILE = "Resources/SamplesConfig.xml";
    int width, height;
    private OutArg<ScriptNode> scriptNode;
    private Context context;
    private DepthGenerator depthGen;
    private ImageGenerator imageGen;
    private byte[] clrbytes;
    private byte[] mask;
    private BufferedImage bimg;
    private boolean basicFrameStored = true;
    private boolean lock;
    private short[] pixelScores;
    private ErodeFilter erodeFilter;
    private byte[] erodedmask;
    private short[] prevDepthValues;
    private Rectangle calibratedArea;
    private int increasedDepth = 0;
    private int decreasedDepth = 0;

    public SimpleViewerCustom() {

        try {
            scriptNode = new OutArg<ScriptNode>();
            context = Context.createFromXmlFile(SAMPLE_XML_FILE, scriptNode);

            depthGen = DepthGenerator.create(context);
            imageGen = ImageGenerator.create(context);
            depthGen.getAlternativeViewpointCapability().setViewpoint(imageGen);
            DepthMetaData depthMD = depthGen.getMetaData();
            ImageMetaData imgMD = imageGen.getMetaData();

            erodeFilter = new ErodeFilter();
            erodeFilter.setIterations(3);

            width = depthMD.getFullXRes();
            height = depthMD.getFullYRes();
            calibratedArea = new Rectangle(width, height);

            prevDepthValues = new short[width * height];
            pixelScores = new short[width * height];
            mask = new byte[width * height];
            erodedmask = new byte[width * height];
            clrbytes = new byte[width * height * 3];

            DataBufferByte dataBuffer = new DataBufferByte(clrbytes, width * height * 3);
            WritableRaster raster = Raster.createInterleavedRaster(dataBuffer, width, height, width * 3, 3, new int[]{0, 1, 2}, null);
            ColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8}, false, false, ComponentColorModel.OPAQUE, DataBuffer.TYPE_BYTE);

            bimg = new BufferedImage(colorModel, raster, false, null);
            bimg.setData(raster);

        } catch (GeneralException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    void updateDepth() {
        try {
            context.waitAnyUpdateAll();
            DepthMetaData depthMD = depthGen.getMetaData();
            ImageMetaData imageMD = imageGen.getMetaData();

            ShortBuffer depth = depthMD.getData().createShortBuffer();
            ByteBuffer img = imageMD.getData().createByteBuffer();
            //depth.rewind();
            //img.rewind();
            if (!basicFrameStored) {
                int counter = 0;
                while (depth.remaining() > 0) {
                    prevDepthValues[counter++] = depth.get();
                }
                basicFrameStored = true;
                return;
            }

            //build mask based on depth
            while (depth.remaining() > 0) {
                int pos = depth.position();
                short pixel = depth.get();

                int difference = prevDepthValues[pos] - pixel;
                if (pos / width < calibratedArea.getMinY() || pos / width > calibratedArea.getMaxY() || pos % width < calibratedArea.getMinX() || pos % width > calibratedArea.getMaxX()) {
                    pixelScores[pos] = 0;
                }
                if (difference > 10 || (pixel == 0 && difference != 0)) {
                    pixelScores[pos] += 1;
                } else if (difference < -10) {
                    pixelScores[pos] -= 1;
                } else
                    pixelScores[pos] = 0;
                //means that object was detected 300 times in a row as being closer than threshold (around 10 secs)
                mask[pos] = (byte) (pixelScores[pos] >= 300 ? 255 : pixelScores[pos] <= -300 ? 127 : 0);
            }

            //clear the mask using erosion
            erodedmask = erodeFilter.erode(width, height, mask);
            increasedDepth = 0;
            decreasedDepth = 0;

            for (int i = 0; i < erodedmask.length; i++) {
                if (erodedmask[i] == (byte) 255) {
                    increasedDepth++;
                    img.get();
                    clrbytes[i * 3 + 0] = (byte)255;
                    clrbytes[i * 3 + 1] = img.get();
                    clrbytes[i * 3 + 2] = img.get();
                } else if (erodedmask[i] == (byte) 127) {
                    decreasedDepth++;
                    img.get();
                    clrbytes[i * 3 + 0] = img.get();
                    clrbytes[i * 3 + 1] = (byte)255;
                    clrbytes[i * 3 + 2] = img.get();
                } else {
                    clrbytes[i * 3 + 0] = img.get();
                    clrbytes[i * 3 + 1] = img.get();
                    clrbytes[i * 3 + 2] = img.get();
                }
            }

        } catch (GeneralException e) {
            e.printStackTrace();
        }
    }

    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }

    public void paint(Graphics g) {

        DataBufferByte dataBuffer = new DataBufferByte(clrbytes, width * height * 3);
        WritableRaster raster = Raster.createInterleavedRaster(dataBuffer, width, height, width * 3, 3, new int[]{0, 1, 2}, null);
        ColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8}, false, false, ComponentColorModel.OPAQUE, DataBuffer.TYPE_BYTE);
        bimg = new BufferedImage(colorModel, raster, false, null);
        bimg.setData(raster);
        g.drawImage(bimg, 0, 0, (int) g.getClipBounds().getWidth(), (int) g.getClipBounds().getHeight(), null);
        if (lock) {
            String text = "COMPUTER LOCKED - CLEAN THE ROOM";
            g.setColor(Color.BLACK);
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 30));
            g.drawString(text, 33, 33);
            g.setColor(Color.WHITE);
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 30));
            g.drawString(text, 30, 30);
        }
    }

    public void calibrate(int left, int right, int top, int bottom) {
        calibratedArea = new Rectangle(left, top, right - left, bottom - top);
        basicFrameStored = false;
    }

    public BufferedImage getBimg() {
        return bimg;
    }

    public boolean isClean() {
        System.out.println(increasedDepth + " " + decreasedDepth);
        return increasedDepth < 200;
    }

    public void lock(boolean lock) {
        this.lock = lock;
    }
}

